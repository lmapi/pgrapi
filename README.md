<div align="center">

# Live@Stock API Suite

## PastureGrowth shared library/API

</div>

### Descrição geral

<div align="justify">

A API Live@Stock.PastureGrowth tem a finalidade de simular o acúmulo potencial de forragem de
(i.e. a diferença entre crescimento e senescência) de pastagens tropicais a partir de séries temporais
de dados meteorológicos. Os dados meteorológicos podem ser obtidos em estações convencionais,
simuladores ou projeções climáticas.

O simulador é baseado no modelo publicado por Pezzopane et al. (2018) e requer dados diários de
radiação solar, temperaturas mínima e máxima do ar e chuva.

A API Live@Stock.PastureGrowth é implementada como uma shared libray para Linux (SO) e
pode ser chamada de qualquer linguagem de programação compatível com essa tecnologia.
Internamente a biblioteca possui um conjunto de funções que empacotam funções-membro de um
objeto da classe Herbage Accumulation, que é instanciada como Singleton no carregamento da SO.
Para configurar a simulação na API Live@Stock.pasture é necessário informar parâmetros e
variáveis de entrada para estabelecer o cenário a ser simulado para em seguida executar a simulação
conforme descrito nas seções a seguir.

<br>

### Configurando os parâmetros do modelo

Os parâmetros para a simulação devem ser passados para função SetParams. Essa função recebe como
parâmetro um objeto da classe vector (da STL do C++) de tipo float.

```c
int SetParams(const vector<float>& params)
```

O objeto vector deve estar corretamente ordenado. A correspondência entre os índices do vetor e os
parâmetros do modelo é definida na Tabela 1.

A função SetParams retorna 0 se os parâmetros forem informados corretamente.

<div align="center">

**Tabela1.** Parâmetros associados aos valores informados em cada índice do vetor de parâmetros



| **IDX**           |**PARAMETRO**  |**DESCRIÇÃO**   |**UNIDADE**    |
|:----------------|:-------------|:--------------|:---------------|
|    0  |       Tb      |  base temperature   |         °C |
|    1  |       TOpt1      | lower optimum temperature   |         °C |
|   2   | TOpt2    | upper optimum temperature |  °C  |
|   3   | Tmax    | maximum temperature for growth |  °C  |
|   4   | k    | light extinction coeficient | -  |
|   5   | SWHC    | soil water holding capacity |  mm  |
|   6   | SWfm    | soil water stress multiplier slope | -  |
|   7   |  SRADtoPAR   | proportion of PAR in solar radiation | -  |
|   8   |  RUEpar   | radiation use efficiency | kg.MJ^-1  |
|   9   |  Parint_max   | maximum intercepted radition | MJ.dia^-1  |
|   10   | H_ETo1    | Eto equation parameter | -  |
|   11   | H_ETo2    | Eto equation parameter | -  |
|   12   | H_ETo3    | Eto equation parameter | -  |
|   13   | LAI    | default leaf area index | -  |

</div>

<br>

### Informando os dados de entrada

Antes de executar a simulação, os vetores com as trajetórias (séries temporais) das variáveis de
entrada deverão ser informadas utilizando-se a função SetInput. Note que o primeiro registro será
associado ao dado meteorológico do primeiro dia de simulação e que os vetores terão de ter
comprimento igual ao tempo total da simulação, i.e., um registro para cada passo de tempo da
simulação. Caso os comprimentos dos vetores estejam diferentes do tempo de duração da simulação,
a simulação não será executada, evitando uma possível violação de acesso de memória (segmentation
fault).

```c
int SetInput(const vector<int> &day,
             const vector<int> &mth,
             const vector<int> &yr,
             const vector<int> &DOY,
             const vector<float> &SRAD,
             const vector<float> &TMAX,
             const vector<float> &TMIN,
             const vector<float> &RAIN)
```

A variável associada a cada vetor é definida conforme a Tabela 2.

<div align="center">

**Tabela 2.** Vetores de dados de entrada. Valores com mesmo índice correspondem ao mesmo registro
temporal.

| **VARIÁVEL**           |**DESCRIÇÃO**  |**UNIDADE**   |
|:----------------|:-------------|:--------------|
| day  | dia do mês | - |
| mth  | indices do mês (1-12) | - |
| yr  | ano (formato yyyy) | - |
| DOY | dia juliano (1/Jan = 1) | - |
| SRAD | Radiação solar incidente | MJ.dia^-1 |
| TMAX | Temperatura máxima diária | °C |
| TMIN | Temperatura mínima diária | °C |
| RAIN | proportion of PAR in solar radiation | mm |

</div>

<br>

### Executando a simulação

A simulação é executada por meio da chamada da função Simulate, que possui a seguinte
assinatura:

```c
int Simulate(const int end_time_par, const float time_step, const float SWCi)
```

Essa função recebe como parâmetros a duração da simulação (dias), o passo de tempo (dias) e o
armazenamento de água no solo (mm) no início da simulação.Se a configuração prévia da simulação não foi realizada corretamente, a chamada da função Simulate
retornará um erro, identificado por um número inteiro de valor 1. Caso a simulação ocorra
corretamente, a função Simulate retorna 0.

<br>

### Resultados

A biblioteca armazena as séries temporais das variáveis de saída ao longo da simulação. O vetor de
uma variável de saída contém seu valor inicial e seus valores em todas as iterações da simulação
sequencialmente. O vetor de uma variável de saída pode ser recuperado por meio da chamada da
função a seguir:

```c
vector<float>* GetOutput(string varName)
```

A função recebe um objeto da classe string, da STL, que corresponde ao nome da variável de saída e
retorna um ponteiro para um objeto da classe vector de tipo float, que contém os valores da variável
de saída para cada passo da simulação.A Tabela 3 contém nomes válidos para as variáveis de saída
da biblioteca Pasture.Growth com sua definição:

<div align="center">

| **VARIÁVEL**           |**DESCRIÇÃO**  |**UNIDADE**   |
|:----------------|:-------------|:--------------|
| "TIME" | Tempo de simulação | dias |
| "HAR" | Taxa potencial de acúmulo de forragem com manejo de sequeiro, i.e. sem irrigação |  kg.ha^-1.dia^-1 |
| "HAR_IRRIG" | Taxa potencial de acúmulo de forragem sem restrição hídrica |  kg.ha^-1.dia^-1 |
| "HAR_PAR_LIMITED" | Taxa potencial de acúmulo de forragem somente limitada pela radiação, i.e. sem limitação hídrica ou de temperatura. Serve apenas como uma função diagnóstico de fatores associados à sazonalidade.| kg.ha^-1.dia^-1 |
| "SWC" | Água armazenada no solo | mm |
| "SW_MULT" | Fator de redução de produtividade devido à restrição hídrica (entre 0 e 1) | - |
| "T_F" | Fator de redução de produtividade devido à temperatura (entre 0 e 1)| - |
| "TPAR_MULT" | Fator de redução de produtividade devido à intensidade de radiação fotossinteticamente ativa (entre 0 e 1) | - |
| "IRRIG" | Quantidade de água de irrigação requerida para zerar o déficit hídrico | mm.dia^-1 | 

</div>

<br>

**Referências Bibliográficas** 

Pezzopane, J. R. M., Santos, P. M., da Cruz, P. G., Bosi, C., & Sentelhas, P. C. (2018). An integrated
agrometeorological model to simulate Marandu palisade grass productivity. Field crops research, 224, 13-21.

</div>
